from MoinMoin.Page import Page
from MoinMoin import wikiutil

suites = set(['oldoldstable', 'oldstable', 'stable', 'testing', 'unstable', 'experimental'])

def execute(macro, suite):
	if suite in suites:
		with open('/srv/wiki.debian.org/var/apt/%s.codename' % suite, 'r') as codename_file:
			request = macro.request
			page = macro.formatter.page
			codename = codename_file.read().strip().title()
			pagename = 'Debian' + codename
			lang = request.cfg.language_default
			lang = page.pi.get('language', lang)
			if not lang or lang == 'en' or lang.startswith('en_'):
				lang = None
			lang_pagename = filter(None, (lang, pagename))
			lang_pagename = '/'.join(lang_pagename)
			if lang and Page(request, lang_pagename).exists():
				pagename = lang_pagename
			return \
				macro.formatter.pagelink(1, pagename) + \
				macro.formatter.text(codename) + \
				macro.formatter.pagelink(0, pagename)
	return ''
