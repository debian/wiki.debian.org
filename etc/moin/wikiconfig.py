# -*- coding: utf-8 -*-

import os.path
import socket
import string
import sys
import re

from MoinMoin.config.multiconfig import DefaultConfig
from MoinMoin.security import Permissions

if sys.version_info < (2, 6):
    null_translate_table = ''.join(chr(i) for i in xrange(256))
else:
    null_translate_table = None

# Load files, stripping anything after a # comment character and
# optionally validating the input via a user-supplied function
def load_file(path, validator=None):
    items = []
    if os.path.exists(path):
        f = open(path)
        for item in f.readlines():
            line = item.strip()
            if line:
                out = re.sub(r'\s*#.*$', '', line)
                if len(out) > 0:
                    if validator:
                        if validator(out):
                            items.append(out)
                    else:
                        items.append(out)
    return items

def is_valid_ipv4_address(address):
    try:
        addr = socket.inet_pton(socket.AF_INET, address)
    except AttributeError:
        try:
            addr = socket.inet_aton(address)
        except socket.error:
            return False
    except socket.error:
        return False
    return True

def is_valid_ipv6_address(address):
    try:
        addr = socket.inet_pton(socket.AF_INET6, address)
    except socket.error:
        return False
    return True

def is_valid_ipv4_net(address):
    if address[-1] != '.':
        return False
    return True

def is_valid_ip(ip):
    return is_valid_ipv4_address(ip) or is_valid_ipv6_address(ip) or is_valid_ipv4_net(ip)

def load_ip_blacklists():
    wlfiles = ['/srv/wiki.debian.org/etc/moin/networks_allow',
              '/srv/wiki.debian.org/etc/moin/hosts_allow',
              '/srv/wiki.debian.org/etc/moin/hosts_allow_auto']
    blfiles = ['/srv/wiki.debian.org/etc/moin/networks_deny',
               '/srv/wiki.debian.org/etc/moin/hosts_deny',
               '/srv/wiki.debian.org/etc/moin/hosts_deny_auto']
    whitelist = set()
    for wlfile in wlfiles:
        whitelist.update(set(load_file(wlfile, is_valid_ip)))
    blacklist = set()
    for blfile in blfiles:
        addresses = set(load_file(blfile, is_valid_ip))
        addresses.difference_update(whitelist)
        blacklist.update(addresses)
    return list(blacklist)

def load_spam_usernames():
    return load_file('/srv/wiki.debian.org/etc/moin/spam_usernames')

spam_usernames = load_spam_usernames()

class Config(DefaultConfig):
    surge_action_limits = None

    # Superusers

    superuser = [u"ErinnClark", u"PaulWise", u"FranklinPiat", u"SteveMcIntyre"]

    log_reverse_dns_lookups = False

    # Wiki identity ----------------------------------------------------

    sitename = u'Debian Wiki'

    interwikiname = None

    page_front_page = u"FrontPage"

    # Critical setup  ---------------------------------------------------

    data_dir = '/srv/wiki.debian.org/var/moin/data/'

    data_underlay_dir = '/srv/wiki.debian.org/var/moin/underlay/'

    url_prefix_local = '/htdocs'
    url_prefix_static = '/htdocs'

    shared_intermap = '/usr/share/moin/data/intermap.txt'

    # Security ----------------------------------------------------------

    # Added to mitigate unspecified security issues:
    # https://moinmo.in/4ct10n/diff/SecurityFixes?action=diff&rev1=42&rev2=43
    actions_excluded = ['xmlrpc', 'MyPages', 'CopyPage', 'SyncPages', 'twikidraw', 'anywikidraw', ]

    acl_hierarchic = True
    acl_rights_default = "Trusted:read,write,delete,revert \
                         Known:read,write,delete,revert \
                         All:read"
    acl_rights_before = "+ErinnClark,PaulWise,FranklinPiat,SteveMcIntyre:admin,read,write,delete,revert"
    acl_rights_after = "All:read"

    # Spammers
    hosts_deny = load_ip_blacklists()

    def password_checker(request, username, passwd):
        for u in spam_usernames:
            if username[:len(u)] == u or u in username:
                return 'Please choose another username, your choice was spammy'
# Disabled by Sledge - stopping password resets on valid accounts
#            if username[0].islower() and username[1].isupper() and len(str(username).translate(null_translate_table, string.ascii_lowercase+string.digits+string.whitespace)) == 2:
#                return 'Please choose another username, your choice might be spammy'
#            if username[0].isupper() and username[1].isupper() and len(str(username)) >= 4:
#                return 'Please choose another username, your choice might be spammy'
#            if re.match(r'[a-z]+_[a-z]+[0-9]+',username):
#                return 'Please choose another username, your choice might be spammy'
        return None

    password_checker = staticmethod(password_checker)

    from MoinMoin.security.antispam import SecurityPolicy

    # Mail --------------------------------------------------------------

    mail_smarthost = "localhost"

    mail_from = "Debian Wiki <wiki@debian.org>"

    mail_login = ""


    # User interface ---------------------------------------------------=

    navi_bar = [
        u'%(page_front_page)s',
        u'RecentChanges',
        u'FindPage',
        u'HelpContents',
    ]

    theme_default = 'debwiki'

    # Language options --------------------------------------------------

    language_default = 'en'

    # Content options ---------------------------------------------------

    show_hosts = 0

    show_section_numbers = 0

    chart_options = {'width': 600, 'height': 300}

    show_rename_redirect = True

    # Search options ---------------------------------------------
    xapian_search = True
    xapian_stemming = True

    # Site specific options ---------------------------------------------

    # Enable the question mark for non-existing pagelinks by default
    user_checkbox_defaults = {'show_nonexist_qm': 1}

    # Recaptcha configuration. Used only at account creation time for now
#    recaptcha_private_key = load_file('/srv/wiki.debian.org/etc/moin/recaptcha_private.key')[0]
#    recaptcha_public_key = load_file('/srv/wiki.debian.org/etc/moin/recaptcha_public.key')[0]

    # Require that new accounts are verified (by clicking on a link in
    # their email) before they can be used. More anti-spam effort
    require_email_verification = True

    # Call out to a script to allow/deny creation of a new
    # account. Let's stop the spammers...
    external_creation_check = "/srv/wiki.debian.org/bin/new-account-check"

    # [JT - 2005-09-27]
    bang_meta = 1

    html_head = '<link rel="shortcut icon" href="/htdocs/favicon.ico">\n<script type="text/javascript" src="/htdocs/bugstatus.js"></script>\n'

    stylesheets = [
        ('all', '/htdocs/debian-wiki-1.0.css')
    ]

    history_count = (10000, 100000)

    # The GUI WYSIWYG editor is not installed with Debian.
    # See /usr/share/doc/python-moinmoin/README.Debian for more info
    editor_force = True
    editor_default = 'text'  # internal default, just for completeness

#    log_timing = True
#    show_timings = True

    # Page credits
    page_credits = [
        # Debian related links
        'Debian '
        '<a href="https://www.debian.org/legal/privacy">privacy policy</a>, '
        'Wiki <a href="/Teams/DebianWiki">team</a>, '
        '<a href="https://bugs.debian.org/wiki.debian.org">bugs</a> and '
        '<a href="https://salsa.debian.org/debian/wiki.debian.org">config</a>.',

        # Thank external things the wiki relies on
        'Powered by '
        '<a href="https://moinmo.in/" title="This site uses the MoinMoin Wiki software.">MoinMoin</a> and '
        '<a href="https://moinmo.in/Python" title="MoinMoin is written in Python.">Python</a>, with '
        'hosting provided by <a href="https://www.man-da.de/">Metropolitan Area Network Darmstadt</a>.'
        ]

    # Options useful for migration
#    page_header1 = """<table><tbody><tr><td style="background:#fff0f0; text-align: center"><span class="anchor" id="line-2"></span><p class="line891">wiki.debian.org is being upgraded or migrated to a new server, please be patient.</td></tr></tbody></table><br/>"""
#    acl_rights_default = 'Known:read All:read'
#    class SecurityPolicy(Permissions):
#        def save(self, editor, newtext, rev, **kw):
#            raise editor.SaveError("wiki.debian.org is being upgraded or migrated to a new server, please be patient.")
#            return self.request.user.valid
