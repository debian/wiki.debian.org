#!/usr/bin/perl -w

use strict;
use POSIX qw(mktime);
use List::MoreUtils qw(uniq);
use Data::Dumper;
use Getopt::Long;

my $base = "/srv/wiki.debian.org";
my $logdir = "$base/var/log";
my $logfile = "$logdir/account-creation-check";
my $blocklogfile = "$logdir/monitor-block";
my $etcdir = "$base/etc/moin";
my $flattened_email_domains = "$etcdir/flattened_email_domains";
my $abused_email_domains = "$etcdir/abused_email_domains";
my @hosts_deny_files = ("$etcdir/hosts_deny_auto",
			"$etcdir/hosts_deny",
			"$etcdir/networks_deny");
my @hosts_allow_files = ("$etcdir/hosts_allow_auto",
			"$etcdir/hosts_allow",
			 "$etcdir/hosts_whitelist");
my @email_deny_files = ("$etcdir/spam_emails",
			"$etcdir/spam_email_domains");
my @email_whitelist_files = ("$etcdir/email_whitelist");

my $num_lines = 20;
my $attempts = 0;
my $printed = 0;
my $ip;
my $email;
my $name;
my $rdns = "";
my $h_allow = 0;
my $e_allow = 0;
my $h_deny = 0;
my $e_deny = 0;
my $h_block = 0;
my $e_block = 0;
my $h_count = 0;
my $e_count = 0;
my $recent = "";
my $old_date = 0;
my $howfar;
my $string_start = "";
my %host_block;
my %host_data;
my @emails_this_host;
my @names_this_host;
my @scores_this_host;
my %email_block;
my %email_data;
my @hosts_this_email;
my @names_this_email;
my @scores_this_email;
my $log_time = 0;
my $autoblock = 0;
my %email_white;
my %host_allow;
my %flatten_domains;
my %abused_domains;

GetOptions ("lines=i"     => \$num_lines,
	    "autoblock=i" => \$autoblock,
            "back=i"      => \$howfar);

if (defined $howfar) {
    $old_date = time() - $howfar;
}
$string_start = gmtime($old_date);

sub print_load_avg()
{
    my ($fh);
    open($fh, "< /proc/loadavg") or die "Can't open /proc/loadavg: $!\n";
    while (my $line = <$fh>) {
	chomp $line;
	print "LOAD: $line\n";
    }
    close($fh);
}

sub mean_score(@)
{
    my $total = 0;
    if (@_ == 0) {
	return 0;
    }
    foreach my $value(@_) {
	$total += $value;
    }
    return int($total / @_);
}

sub get_time()
{
    my @tm;
    my $text;

    @tm = gmtime();
    $text = sprintf("%4d-%02d-%02d %02d:%02d:%02d UTC",
                    (1900 + $tm[5]),(1 + $tm[4]),$tm[3],$tm[2],$tm[1],$tm[0]);
    return $text;
}

sub date_to_time_t($) {
    my $date = shift;
    if ($date =~ /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/) {
        return mktime($6, $5, $4, $3, $2 - 1, $1 - 1900, 0, 0, 0);
    }
}

sub is_abused_email_domain($) {
    my $email = shift;
    if ($email =~ m/\S+\@(\S+)/) {
	my $domain = $1;
	if ($abused_domains{$domain}) {
	    return 1;
	}
    }
    return 0;
}

sub should_flatten_email($) {
    my $email = shift;
    if ($email =~ m/\S+\@(\S+)/) {
	my $domain = $1;
	if ($flatten_domains{$domain}) {
	    return 1;
	}
    }
    return 0;
}

sub flatten_email($) {
    my $email = shift;
    my @email_parts = split /\@/,$email;
    $email_parts[0] =~ s/\.//g;
    $email = "$email_parts[0]\@$email_parts[1]";
    return $email;
}

sub add_host_entry {
    my %entry;
    my $ip = shift;
    my $tmp1;
    my $tmp2;
    my $tmp3;
    my @emails;
    my @names;
    my @scores;
    $entry{"rdns"} = shift;
    $entry{"count"} = shift;
    $entry{"allow"} = shift;
    $entry{"deny"} = shift;
    $entry{"block"} = shift;
    $entry{"recent"} = shift;
    $entry{"recent_time_t"} = shift;

    $tmp1 = shift;
    push(@emails, @$tmp1);
    $entry{"emails"} = \@emails;

    $tmp2 = shift;
    push(@names, @$tmp2);
    $entry{"names"} = \@names;

    $tmp3 = shift;
    push(@scores, @$tmp3);
    $entry{"scores"} = \@scores;

    $host_data{"$ip"} = \%entry;
}

sub add_email_entry {
    my %entry;
    my $email = shift;
    my $tmp1;
    my $tmp2;
    my $tmp3;
    my @hosts;
    my @names;
    my @scores;
    $entry{"count"} = shift;
    $entry{"allow"} = shift;
    $entry{"deny"} = shift;
    $entry{"block"} = shift;
    $entry{"recent"} = shift;
    $entry{"recent_time_t"} = shift;

    $tmp1 = shift;
    push(@hosts, @$tmp1);
    $entry{"hosts"} = \@hosts;

    $tmp2 = shift;
    push(@names, @$tmp2);
    $entry{"names"} = \@names;

    $tmp3 = shift;
    push(@scores, @$tmp3);
    $entry{"scores"} = \@scores;

    $email_data{"$email"} = \%entry;
}

sub host_sort_func {
    my $tmp_email_a = $host_data{$a}{"emails"};
    my $tmp_email_b = $host_data{$b}{"emails"};
    my $tmp_names_a = $host_data{$a}{"names"};
    my $tmp_names_b = $host_data{$b}{"names"};
    my $tmp_scores_a = $host_data{$a}{"scores"};
    my $tmp_scores_b = $host_data{$b}{"scores"};
    my @emails_a = @$tmp_email_a;
    my @emails_b = @$tmp_email_b;
    my @names_a = @$tmp_names_a;
    my @names_b = @$tmp_names_b;
    my @scores_a = @$tmp_scores_a;
    my @scores_b = @$tmp_scores_b;
    $host_data{$b}{"count"} <=> $host_data{$a}{"count"}
    ||
	$host_data{$a}{"allow"} <=> $host_data{$b}{"allow"}
    ||
	scalar(uniq @emails_b) <=> scalar (uniq @emails_a)
    ||
	scalar(uniq @names_b) <=> scalar (uniq @names_a)
    ||
	mean_score(@scores_b) <=> mean_score(@scores_a)
    ||
	$host_data{$b}{"recent_time_t"} <=> $host_data{$a}{"recent_time_t"};
}

sub email_sort_func {
    my $tmp_hosts_a = $email_data{$a}{"hosts"};
    my $tmp_hosts_b = $email_data{$b}{"hosts"};
    my $tmp_names_a = $email_data{$a}{"names"};
    my $tmp_names_b = $email_data{$b}{"names"};
    my $tmp_scores_a = $email_data{$a}{"scores"};
    my $tmp_scores_b = $email_data{$b}{"scores"};
    my @hosts_a = @$tmp_hosts_a;
    my @hosts_b = @$tmp_hosts_b;
    my @names_a = @$tmp_names_a;
    my @names_b = @$tmp_names_b;
    my @scores_a = @$tmp_scores_a;
    my @scores_b = @$tmp_scores_b;
    $email_data{$b}{"count"} <=> $email_data{$a}{"count"}
    ||
	$email_data{$a}{"allow"} <=> $email_data{$b}{"allow"}
    ||
	scalar(uniq @hosts_b) <=> scalar (uniq @hosts_a)
    ||
	scalar(uniq @names_b) <=> scalar (uniq @names_a)
    ||
	mean_score(@scores_b) <=> mean_score(@scores_a)
    ||
        $email_data{$b}{"recent_time_t"} <=> $email_data{$a}{"recent_time_t"};

}

# Read the deny files first

open (FLAT, "< $flattened_email_domains") or die "Unable to open $flattened_email_domains for reading: $!\n";
while (my $line = <FLAT>) {
    chomp $line;
    # Lowercase at this point
    $line = lc $line;
    $line =~ s/\s*#.*$//;
    if (length($line) > 2) {
	$flatten_domains{$line}++;
    }
}
close FLAT;

open (ABUSED, "< $abused_email_domains") or die "Unable to open $abused_email_domains for reading: $!\n";
while (my $line = <ABUSED>) {
    chomp $line;
    # Lowercase at this point
    $line = lc $line;
    $line =~ s/\s*#.*$//;
    if (length($line) > 2) {
	$abused_domains{$line}++;
    }
}
close ABUSED;

foreach my $denyfile (@hosts_deny_files) {
    open (DENY, "< $denyfile") or die "Unable to open $denyfile for reading: $!\n";
    while (my $line = <DENY>) {
	chomp $line;
	# Lowercase at this point
	$line = lc $line;
	$line =~ s/\s*#.*$//;
	if (length($line) > 2) {
	    $host_block{$line}++;
	}
    }
    close DENY;
}
foreach my $denyfile (@email_deny_files) {
    open (DENY, "< $denyfile") or die "Unable to open $denyfile for reading: $!\n";
    while (my $line = <DENY>) {
	chomp $line;
	# Lowercase at this point
	$line = lc $line;
	$line =~ s/\s*#.*$//;
	if (length($line) > 2) {
	    if (should_flatten_email($line)) {
		$line = flatten_email($line);
	    }
	    $email_block{$line}++;
	}
    }
    close DENY;
}
foreach my $denyfile (@email_whitelist_files) {
    open (DENY, "< $denyfile") or die "Unable to open $denyfile for reading: $!\n";
    while (my $line = <DENY>) {
	chomp $line;
	# Lowercase at this point
	$line = lc $line;
	$line =~ s/\s*#.*$//;
	if (length($line) > 2) {
	    $email_white{$line}++;
	}
    }
    close DENY;
}
foreach my $denyfile (@hosts_allow_files) {
    open (DENY, "< $denyfile") or die "Unable to open $denyfile for reading: $!\n";
    while (my $line = <DENY>) {
	chomp $line;
	# Lowercase at this point
	$line = lc $line;
	$line =~ s/\s*#.*$//;
	if (length($line) > 2) {
	    $host_allow{$line}++;
	}
    }
    close DENY;
}

open (LOG, "< $logfile") or die "Unable to open $logfile for reading: $!\n";
while (my $line = <LOG>) {
    chomp $line;
    my %entry;

    if ($line =~ m/^(\d\d\d\d-\d\d-\d\d\ \d\d:\d\d:\d\d) UTC: New account attempt/) {
	$log_time = date_to_time_t($1);
	if ($log_time >= $old_date) {
	    $attempts++;
	    if (defined($ip)) {
		add_host_entry($ip, $rdns, $h_count, $h_allow, $h_deny, $h_block, $recent, $log_time, \@emails_this_host, \@names_this_host, \@scores_this_host);
		undef $ip;
	    }
	    if (defined($email)) {
		add_email_entry($email, $e_count, $e_allow, $e_deny, $e_block, $recent, $log_time, \@hosts_this_email, \@names_this_email, \@scores_this_email);
		undef $email;
	    }
	}
	$recent = $1;
	@names_this_email = ();
	@names_this_host = ();
	@scores_this_email = ();
	@scores_this_host = ();
    } elsif ($line =~ m/username:\s+(\S+.*$)/) {
	$name = $1;
    } elsif ($line =~ m/email:\s+(.*)/) {
	# Lowercase at this point
	$email = lc $1;
	$e_count = 1;
	$e_allow = 0;
	$e_deny = 0;
	@hosts_this_email = ();
	$e_block = 0;
	if (should_flatten_email($email)) {
	    $email = flatten_email($email);
	}
	if ($email_data{"$email"}) {
	    $e_allow = $email_data{"$email"}{"allow"};
	    $e_count = $email_data{"$email"}{"count"} + 1;
	    $e_deny  = $email_data{"$email"}{"deny"};
	    $e_block = $email_data{"$email"}{"block"};
	    my $tmp1 = $email_data{"$email"}{"hosts"};
	    @hosts_this_email = @$tmp1;
	    my $tmp2 = $email_data{"$email"}{"names"};
	    @names_this_email = @$tmp2;
	    my $tmp3 = $email_data{"$email"}{"scores"};
	    @scores_this_email = @$tmp3;
	}
	push (@names_this_email, $name);
    } elsif ($line =~ m/host:\s+(\S+)\s+\((.*)\)/) {
	$ip = $1;
	$rdns = $2;
	$h_count = 1;
	$h_allow = 0;
	$h_deny = 0;
	$h_block = 0;
	@emails_this_host = ();
	if ($host_data{"$ip"}) {
	    $h_allow = $host_data{"$ip"}{"allow"};
	    $h_count = $host_data{"$ip"}{"count"} + 1;
	    $h_deny  = $host_data{"$ip"}{"deny"};
	    $h_block = $host_data{"$ip"}{"block"};
	    my $tmp1 = $host_data{"$ip"}{"emails"};
	    @emails_this_host = @$tmp1;
	    my $tmp2 = $host_data{"$ip"}{"names"};
	    @names_this_host = @$tmp2;
	    my $tmp3 = $host_data{"$ip"}{"scores"};
	    @scores_this_host = @$tmp3;
	}
	push (@emails_this_host, $email);
	push (@hosts_this_email, $ip);
	push (@names_this_host, $name);
    } elsif ($line =~ m/ALLOW:\s+total score (-*\d+)/) {
	my $score = $1;
	$h_allow++;
	$e_allow++;
	push (@scores_this_host, $score);
	push (@scores_this_email, $score);
    } elsif ($line =~ m/DENY:\s+total score (-*\d+)/) {
	my $score = $1;
	$h_deny++;
	$e_deny++;
	push (@scores_this_host, $score);
	push (@scores_this_email, $score);

    } elsif ($line =~ m/BLOCK:/) {
	$h_block++;
	$e_block++;
    }
}

if (defined($ip)) {
    add_host_entry($ip, $rdns, $h_count, $h_allow, $h_deny, $h_block, $recent, $log_time, \@emails_this_host, \@names_this_host, \@scores_this_host);
}
if (defined($email)) {
    add_email_entry($email, $e_count, $e_allow, $e_deny, $e_block, $recent, $log_time, \@hosts_this_email, \@names_this_email, \@scores_this_email);
}

close(LOG);

my @sorted = sort host_sort_func keys %host_data;

if (!$autoblock) {

    print_load_avg();

    print "#ATT MOST RECENT         ALL/DEN/SCO EMAILS  NAMES  IP (NAME),      attempts: $attempts, hosts logged: " . scalar(keys %host_data) . " since $string_start\n";
}
foreach my $ip (@sorted) {
    my $ignored = 0;

    # If the host is blocked already, ignore it
    $ignored = $host_data{"$ip"}{"block"};
    if ($host_block{"$ip"}) {
	$ignored += $host_block{"$ip"};
    }
    if (!$ignored and $ip =~ m/((\d+\.\d+\.)\d+\.)\d+/) { # ipv4
	my $net_b = $1;
	my $net_c = $2;
	if ($host_block{$net_b} or $host_block{$net_c}) {
	    $ignored = 1;
	}
    }

    # If the host is explicitly allowed, ignore it
    if ($host_allow{"$ip"}) {
	$ignored = 1;
    }

    # If one of the emails we've seen for this host is on the email
    # whitelist, ignore it too
    if (!$ignored) {
	my $tmp1 = $host_data{"$ip"}{"emails"};
	my @emails_this_host = @$tmp1;
	foreach my $email (@emails_this_host) {
	    if ($email_white{$email}) {
		$ignored = 1;
	    }
	}
    }

    # If we've not found a reason to ignore the host above, it's a
    # valid thing to display
    if (!$ignored) {
	my $tmp1 = $host_data{"$ip"}{"emails"};
	my @emails_this_host = @$tmp1;
	my @uniq_emails = uniq @emails_this_host;

	my $tmp2 = $host_data{"$ip"}{"names"};
	my @names_this_host = @$tmp2;
	my @uniq_names = uniq @names_this_host;

	my $tmp3 = $host_data{"$ip"}{"scores"};
	my @scores_this_host = @$tmp3;
	my $mean_score = mean_score(@scores_this_host);

	if ($autoblock) {
	    # Should we autoblock the IP?
	    if ( ($host_data{"$ip"}{"count"} >= 2)) {
		if (
		    (scalar(@uniq_emails) >= 4) or
		    (scalar(@uniq_names) >= 4) or
		    ((scalar(@uniq_emails) >= 3) and (scalar(@uniq_names) >= 3)) or
		    ((scalar(@uniq_emails) >= 2) and (scalar(@uniq_names) >= 2) and ($mean_score >= 30))
		    ) {
		    my $date = get_time();
		    open (LOG, ">> $blocklogfile")
			or die "Can't write to $blocklogfile: $!\n";
		    printf LOG "%s: Auto-block IP %s (%s)\n", $date, $ip, $host_data{"$ip"}{"rdns"};
		    printf LOG "  attempts: %d\n", $host_data{"$ip"}{"count"};
		    printf LOG "  most recent: %s\n", $host_data{"$ip"}{"recent"};
		    printf LOG "  allowed: %d\n", $host_data{"$ip"}{"allow"};
		    printf LOG "  denied: %d\n", $host_data{"$ip"}{"deny"};
		    printf LOG "  unique emails (%d):", scalar(@uniq_emails);
		    foreach my $email (@uniq_emails) {
			printf LOG " $email";
		    }
		    printf LOG "\n";
		    printf LOG "  unique names (%d):", scalar(@uniq_names);
		    foreach my $name (@uniq_names) {
			printf LOG " $name";
		    }
		    printf LOG "\n";
		    printf LOG "  mean score (%d):", $mean_score;
		    foreach my $score (@scores_this_host) {
			printf LOG " $score";
		    }
		    printf LOG "\n";
		    close LOG;
		    system ("/srv/wiki.debian.org/bin/block-ip", "--quiet", "--reason", "monitor", $ip);
		}
	    }
	} else {
	    printf "% 4d ", $host_data{"$ip"}{"count"};
	    printf "% s ", $host_data{"$ip"}{"recent"};
	    printf "%3d ", $host_data{"$ip"}{"allow"};
	    printf "%3d ", $host_data{"$ip"}{"deny"};
	    printf "%3d ", $mean_score;
	    printf "   %3d  ", scalar(@uniq_emails);
	    printf "%5d  ", scalar(@uniq_names);
	    print  $ip . " (" . $host_data{"$ip"}{"rdns"} . ")\n";
	    print  "     E: @uniq_emails N: @uniq_names\n";
	    $printed++;
	}
    }
    if ($printed >= $num_lines) {
	last;
    }
}

@sorted = sort email_sort_func keys %email_data;
if (!$autoblock) {
    print "\n#ATT MOST RECENT         ALL/DEN/SCO  HOSTS  NAMES  EMAIL,       attempts: $attempts, email addresses logged: " . scalar(keys %email_data) . " since $string_start\n";
}
$printed = 0;
foreach my $email (@sorted) {
    my $ignored = 0;
    my $email_flat = $email;

    $ignored = $email_data{"$email"}{"block"};

    # If the email is blocked already, ignore it
    if ($email_block{"$email"}) {
	$ignored += $email_block{"$email"};
    }

    # If a flattened version of the email is blocked already, ignore it
    if (!$ignored) {
	$email_flat =~ s/\?.*//;
	if ($email_block{"$email_flat"}) {
	    $ignored += $email_block{"$email_flat"};
	}
    }

    # If the email domain is blocked already, ignore it
    if (!$ignored and $email =~ m/\S+\@(\S+)/) {
	my $domain = $1;
	if ($email_block{$domain}) {
	    $ignored = 1;
	}
    }

    # If the email is on our whitelist, ignore it too
    if (!$ignored) {
	if ($email_white{$email}) {
            $ignored = 1;
        }
    }

    # If one of the hosts we've seen for this email is on the host
    # allow list, ignore it too
    if (!$ignored) {
	my $tmp1 = $email_data{"$email"}{"hosts"};
	my @hosts_this_email = @$tmp1;
	foreach my $ip (@hosts_this_email) {
	    if ($host_allow{$ip}) {
		$ignored = 1;
	    }
	}
    }

    # If the "email" contains special characters, ignore it - don't
    # block the whole world
    if (!$ignored) {
	if ($email =~ m/[\,\\\/\|]/) {
	    $ignored = 1;
	}
    }

    # If we've not found a reason to ignore the email above, it's a
    # valid thing to display
    if (!$ignored) {
	my $tmp1 = $email_data{"$email"}{"hosts"};
	my @hosts_this_email = @$tmp1;
	my @uniq_hosts = uniq @hosts_this_email;

	my $tmp2 = $email_data{"$email"}{"names"};
	my @names_this_email = @$tmp2;
	my @uniq_names = uniq @names_this_email;

	my $tmp3 = $email_data{"$email"}{"scores"};
	my @scores_this_email = @$tmp3;
	my $mean_score = mean_score(@scores_this_email);

	if ($autoblock) {
	    # Should we autoblock the email?
	    if ( ($email_data{"$email"}{"count"} >= 2)) {
		if (
		    (scalar(@uniq_hosts) >= 4) or
		    (scalar(@uniq_names) >= 4) or
		    ((scalar(@uniq_hosts) >= 3) and (scalar(@uniq_names) >= 3)) or
		    ((scalar(@uniq_hosts) >= 2) and is_abused_email_domain($email)) or
		    ((scalar(@uniq_hosts) >= 2) and (scalar(@uniq_names) >= 2) and ($mean_score >= 30))
		    ) {
		    my $date = get_time();
		    open (LOG, ">> $blocklogfile")
			or die "Can't write to $blocklogfile: $!\n";
		    printf LOG "%s: Auto-block email %s and associated IPs\n", $date, $email;
		    printf LOG "  attempts: %d\n", $email_data{"$email"}{"count"};
		    printf LOG "  most recent: %s\n", $email_data{"$email"}{"recent"};
		    printf LOG "  allowed: %d\n", $email_data{"$email"}{"allow"};
		    printf LOG "  denied: %d\n", $email_data{"$email"}{"deny"};
		    printf LOG "  abused_domain: %d\n", is_abused_email_domain($email);
		    printf LOG "  unique hosts (%d):", scalar(@uniq_hosts);
		    foreach my $host (@uniq_hosts) {
			printf LOG " $host";
		    }
		    printf LOG "\n";
		    printf LOG "  unique names (%d):", scalar(@uniq_names);
		    foreach my $name (@uniq_names) {
			printf LOG " $name";
		    }
		    printf LOG "\n";
		    printf LOG "  mean score (%d):", $mean_score;
		    foreach my $score (@scores_this_email) {
			printf LOG " $score";
		    }
		    printf LOG "\n";
		    $email =~ s/\?.*//;
		    system ("/srv/wiki.debian.org/bin/block-email", "--quiet", "--reason", "monitor", $email);
		    system ("/srv/wiki.debian.org/bin/block-ip", "--quiet", "--reason", "monitor", @uniq_hosts);
		    close LOG;
		}
	    }
	} else {
	    printf "% 4d ", $email_data{"$email"}{"count"};
	    printf "% s ", $email_data{"$email"}{"recent"};
	    printf "%3d ", $email_data{"$email"}{"allow"};
	    printf "%3d ", $email_data{"$email"}{"deny"};
	    printf "%3d ", $mean_score;
	    printf "   %3d  ", scalar(@uniq_hosts);
	    printf "%5d  ", scalar(@uniq_names);
	    print  "$email\n";
	    print  "     H: @uniq_hosts N: @uniq_names\n";
	    $printed++;
	}
    }
    if ($printed >= $num_lines) {
	last;
    }
}


