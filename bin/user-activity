#!/usr/bin/perl -w
#
# Perl script to track user activity and push details to contributors.d.o
#
# Copyright 2013-2021 Steve McIntyre <93sam@debian.org>
# GPL V2 or later

use strict;
use File::Temp qw/ tempfile tempdir /;
use Socket;
use JSON;
use Data::Dumper;
use Getopt::Long;
Getopt::Long::Configure ('no_ignore_case');
Getopt::Long::Configure ('no_auto_abbrev');

my $base = "/srv/wiki.debian.org";
my $log = "$base/var/moin/data";
my $etc = "$base/etc/moin";
my $hosts_deny = "$etc/hosts_deny";
my $email_deny = "$etc/spam_emails";
my $auth_token;
my $submitted = 0;
my $spammer;
my $date;
my %accounts;
my %emails;
my $json_text;
my $error = 0;
my %sources = (
    "wiki" => {
	"key_file" => "$etc/contributors.key",
	"cdo_source" => "wiki.debian.org",
	"filter" => "\\S+",
    },
    "release-images" => {
	"key_file" => "$etc/release-images-contributors.key",
	"cdo_source" => "Release image testers",
	"filter" => 'Teams\(2f\)DebianCD\(2f\)ReleaseTesting\\S+',
    },
);

my $nosend = 0;
my $source = "wiki"; # default
my $result = GetOptions("nosend" => \$nosend,
			"source=s" => \$source)
    or die ("Error parsing command line");

if (!$sources{$source}) {
    die "Invalid source name $source!\n";
}

sub get_time()
{
    my @tm;
    my $text;

    @tm = gmtime();
    $text = sprintf("%4d-%02d-%02d %02d:%02d:%02d UTC",
                    (1900 + $tm[5]),(1 + $tm[4]),$tm[3],$tm[2],$tm[1],$tm[0]);
    return $text;
}

# Given a moin-style user account in the form dddddd.dddd.dddd and a
# timestamp, add/update the activity for that account
sub add_account_activity($$)
{
    my $account = shift;
    my $time = shift;
    my $tmp = $accounts{$account};
    my %entry;
    if (defined($tmp)) {
	%entry = %$tmp;
	if ($time < $entry{"begin"}) {
	    $entry{"begin"} = $time;
	}
	if ($time > $entry{"end"}) {
	    $entry{"end"} = $time;
	}
    } else {
	$entry{"begin"} = $time;
	$entry{"end"} = $time;
    }
    $accounts{$account} = \%entry;
}

# Given an email address for a wiki account and a timestamp,
# add/update the activity for that account
sub add_email_activity($$$)
{
    my $email = shift;
    my $username = shift;
    my $time = shift;
    my $tmp = $emails{$email};
    my %entry;
    if (defined($tmp)) {
	%entry = %$tmp;
	if ($time < $entry{"begin"}) {
	    $entry{"begin"} = $time;
	}
	if ($time > $entry{"end"}) {
	    $entry{"end"} = $time;
	}
    } else {
	$entry{"begin"} = $time;
	$entry{"end"} = $time;
    }
    $entry{"username"} = $username;
    $emails{$email} = \%entry;
}

sub convdate($)
{
    my $time = shift;
    $time =~ s/\.(\d)+$//g;
    if (length($time) == 16) {
        $time =~ s/\d\d\d\d\d\d$//;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = 
        gmtime($time);
    return sprintf("%4.4d-%2.2d-%2.2d", $year+1900, $mon+1, $mday);
}

sub lookup_email_name($)
{
    my $user = shift;
    my $ignore = 0;
    my $email = "invalid";
    my $username = "invalid";

    open (USERFILE, "< $user") or return ("$!", 1);
    while (<USERFILE>) {
	chomp;
	m/^quicklinks.*spammer/ and $ignore = 1;
	m/^email=(.+)$/ and $email = $1;
	m/^name=(.+)$/ and $username = $1;
    }
    close USERFILE;

    # If we don't have data, ignore this user account
    if (($email eq "invalid") or ($username eq "invalid")) {
	$ignore = 1;
    }
    return ($email, $ignore, $username);
}

chdir "$base/var/moin/data/user"
    or die "Unable to chdir to moin user data dir: $!\n";

my $filter = $sources{$source}{"filter"};

# Parse logs looking for begin and newset entry for each user account
open (EDITLOG, "zcat -f $log/archived-logs/*/edit-log* $log/edit-log-* $log/edit-log |")
    or die "Unable to open edit logs for reading: $!\n";
while (<EDITLOG>) {
    chomp;
    if (m/^(\d+)\s+\d+\s+\S+\s+$filter\s+\S+\s+\S+\s+(\d+\.\d+\.\d+)/) {
	my $logtime = $1;
	my $account = $2;
	# print "Match on $_\n";
	add_account_activity($account, $logtime);
    }
}
close EDITLOG;

# Look up from user account to email address. There may be several
# accounts for one email, possibly, so merge the data from those
# multiple accounts into one record linked to the email address.
foreach my $user (keys %accounts) {
    my $tmp = $accounts{$user};
    my %entry = %$tmp;
#    printf("account %s: begin entry %d, end entry %d\n", $user, $entry{"begin"}, $entry{"end"});
    my ($email, $ignore, $username) = lookup_email_name($user);
    if ($ignore) {
	# print "ignore $email ($user)!\n";
    } else {
	add_email_activity($email, $username, $entry{"begin"});
	add_email_activity($email, $username, $entry{"end"});
    }
}

$json_text .= "[\n";

# Final pass - work through the set of email addresses and submit
# them. This generates JSON by hand (yes, I know!) as the perl JSON
# libs look like too much hard work for me...
foreach my $email (sort keys %emails) {
    my $tmp = $emails{$email};
    my %entry = %$tmp;
    {
	my $oldtime = convdate($entry{"begin"});
	my $newtime = convdate($entry{"end"});
	my $username = $entry{"username"};
	#print "$email: OLD $oldtime, NEW $newtime\n";
	$json_text .= "  {\n";
	$json_text .= "    \"id\": [ { \"type\":\"email\", \"id\":\"$email\" } ],\n";
	$json_text .= "    \"contributions\": [";
	$json_text .= " { \"type\":\"edit\", \"begin\":\"$oldtime\", \"end\":\"$newtime\", \"url\":\"https://wiki.debian.org/$username\" } ]\n";
	$json_text .= "  },\n";
	$submitted++;
    }
}

# Remove the trailing ",\n" on the last record to stop the far end
# complaining about our format.
$json_text = substr $json_text, 0, -2;
$json_text .= "\n]\n";

# Print out the data and compress it.
my ($fh, $filename) = tempfile('wiki-user-activity-data-XXXXXXXX', SUFFIX => '.json');
print $fh "$json_text\n";
close $fh;

system("gzip --best $filename");

# Submit. Grab the auth token from a file in /etc

my $key_file = $sources{$source}{"key_file"};
open (KEYFILE, "< $key_file") or die "Can't open keyfile $key_file for reading: $!\n";
$auth_token = <KEYFILE>;
chomp $auth_token;
close KEYFILE;

my ($loghandle, $tmplogfile) = tempfile('wiki-user-activity-log-XXXXXXXX');
close $loghandle;

my $form_source = $sources{$source}{"cdo_source"};
my $cmd = "curl ";
$cmd .= "--capath /etc/ssl/ca-debian ";
#$cmd .= "--fail ";
$cmd .= "--form source=\"$form_source\" --form auth_token=\"$auth_token\" ";
$cmd .= "--form data_compression=gzip --form data=\@$filename.gz ";
$cmd .= "--output $tmplogfile --silent --show-error ";
$cmd .= "https://contributors.debian.org/contributors/post";

my $cmd_failed = 0;
if ($nosend) {
    print "Would send $cmd\n";
    print "Data submission in $filename.gz\n";
    exit 0;
} else {
    system($cmd);
    $cmd_failed = $?;
}

$date = get_time();

$json_text = "";
#$tmplogfile = "/tmp/XTrsSjvjcv";
open (INLOG, "< $tmplogfile") or die "Can't open curl logfile $tmplogfile for reading: $!\n";
while (my $line = <INLOG>) {
    $json_text .= $line;
}
close INLOG;

open (OUTLOG, ">> $base/var/log/user-activity.log") or
    die "Can't open $base/var/log/user-activity.log for writing: $!\n";

print OUTLOG "$date: Found $submitted unique contributors from log files for the $source source\n";

my $tmp;
$tmp = from_json($json_text) unless $cmd_failed;
my %r;

if (defined $tmp) {
    %r = %$tmp;
    my $tmp1 = $r{"errors"};
    my $contributors_version = undef;

    # contributors.d.o code changed its interface without warning :-(
    if (defined $tmp1) {
	$contributors_version = 1;
    } else {
	$tmp1 = $r{"stats"};
	if (defined $tmp1) {
	    $contributors_version = 2;
	}
    }

    if (!defined $contributors_version) {
	print OUTLOG "    ERROR! Unable to parse return structure from contributors.debian.org\n";
	die "ERROR! Unable to parse return structure from contributors.debian.org\n";
    } else {
	printf OUTLOG "    Got a version %d response\n", $contributors_version;

	if ($contributors_version == 1) {
	    my $tmp_error = $r{"errors"};
	    my @errors = @$tmp_error;
	    $error = $r{"code"};
	    #print Dumper(%r);
	    printf OUTLOG "    POST returned %d in JSON\n", $error;
	    printf OUTLOG "    identifiers:   skipped %d\n", $r{"identifiers_skipped"};
	    printf OUTLOG "    records:       skipped %d parsed    %d\n", $r{"records_skipped"}, $r{"records_parsed"};
	    printf OUTLOG "    contributions: skipped %d processed %d updated %d created %d\n",
		$r{"contributions_skipped"}, $r{"contributions_processed"},
		$r{"contributions_updated"}, $r{"contributions_created"};
	    printf OUTLOG "    errors:\n";
	    if (0 == scalar(@errors)) {
		printf OUTLOG "        none\n";
	    } else {
		foreach my $e (@errors) {
		    printf OUTLOG "        %s\n", $e;
		}
	    }
	} elsif ($contributors_version == 2) {
	    my $tmp_stats = $r{"stats"};
	    my %stats = %$tmp_stats;
	    my $tmp_errors = $stats{"errors"};
	    my @errors = @$tmp_errors;
	    $error = $r{"result_code"};

	    #print Dumper(%r);
	    printf OUTLOG "    POST returned %d in JSON\n", $error;
	    printf OUTLOG "    records:       parsed    %d\n", $stats{"records_parsed"};
	    printf OUTLOG "    errors:\n";
	    if (0 == scalar(@errors)) {
		printf OUTLOG "        none\n";
	    } else {
		foreach my $e (@errors) {
		    printf OUTLOG "        %s\n", $e;
		}
	    }
	}

	# 200 == OK, 400 == OK with warnings
	if ($error <= 400) {
	    unlink "$filename.gz", "$tmplogfile";
	} else {
	    print OUTLOG "    Major error. Leaving files around: submitted data in $filename.gz, return in $tmplogfile\n";
	    print "    Major error. Leaving files around: submitted data in $filename.gz, return in $tmplogfile\n";
	}
    }

} else {
    print OUTLOG "    POST returned invalid JSON data\n";
    print OUTLOG "    Leaving files around: submitted data in $filename.gz, return in $tmplogfile\n";
    print "    POST returned invalid JSON data\n";
    print "    Leaving files around: submitted data in $filename.gz, return in $tmplogfile\n";
}

close OUTLOG;
